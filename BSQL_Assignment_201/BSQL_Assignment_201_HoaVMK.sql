CREATE DATABASE SQLEssential201;
GO

USE SQLEssential201;
GO

/* Q1.1 */
CREATE TABLE EMPLOYEE (
	EmpNo int PRIMARY KEY,
	EmpName nvarchar(50) NOT NULL,
	BirthDay date NOT NULL,
	DeptNo int NOT NULL,
	MgrNo int NOT NULL,
	StartDate date NOT NULL,
	Salary decimal(18, 2) NOT NULL,
	Level int NOT NULL CHECK (Level between 0 and 7),
	Status int NOT NULL CHECK (Status between 0 and 2),
	Note text
)
GO

/* Q1.2 */
CREATE TABLE SKILL (
	SkillNo int PRIMARY KEY IDENTITY(1, 1),
	SkillName nvarchar(30) NOT NULL,
	Note text
);
GO

/* Q1.3 */
CREATE TABLE DEPARTMENT (
	DeptNo int PRIMARY KEY IDENTITY(1, 1),
	DeptName nvarchar(30) NOT NULL,
	Note text
);
GO

/* Q1.4 */
CREATE TABLE EMP_SKILL (
	SkillNo int NOT NULL FOREIGN KEY REFERENCES SKILL(SkillNo),
	EmpNo int NOT NULL FOREIGN KEY REFERENCES EMPLOYEE(EmpNo),
	SkillLevel int CHECK (SkillLevel between 1 and 3),
	RegDate date NOT NULL,
	Description text,
	PRIMARY KEY(SkillNo, EmpNo)
);
GO

/* Q2.1 */
ALTER TABLE EMPLOYEE
ADD Email nvarchar(255) UNIQUE NOT NULL
GO

/* Q2.2 */
ALTER TABLE EMPLOYEE
ADD CONSTRAINT DK_EMPLOYEE_MgrNo
DEFAULT 0 FOR MgrNo
GO

ALTER TABLE EMPLOYEE
ADD CONSTRAINT DK_EMPLOYEE_Status
DEFAULT 0 FOR Status
GO

/* Q3.1 */
ALTER TABLE EMPLOYEE
ADD CONSTRAINT FK_EMPLOYEE_DEPARTMENT_DeptNo
FOREIGN KEY(DeptNo) REFERENCES DEPARTMENT(DeptNo)
GO

/* Q3.2 */
ALTER TABLE EMP_SKILL
DROP COLUMN Description
GO

/* Q4.1 */

INSERT INTO DEPARTMENT (DeptName)
VALUES ('Development'),
	('HR'),
	('Accounting'),
	('Sales'),
	('Marketing')
GO


INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, StartDate, Salary, Level, Status)
VALUES (1, 'Le Giang', '09-20-1999', 'a@gmail.com', 1, '09-20-2023', 1000, 1, 0),
	(2, 'Tran Long', '12-01-2000', 'b@gmail.com', 1, '09-20-2023', 1000, 1, 0),
	(3, 'Le Hoang', '05-16-2000', 'c@gmail.com',  2, '09-20-2023', 1000, 2, 0),
	(4, 'Nguyen An', '11-01-2000', 'd@gmail.com',3, '09-20-2023', 1000, 1, 0),
	(5, 'Nguyen Phuc', '05-26-2001', 'e@gmail.com', 5, '09-20-2023', 1000, 1, 0),
	(6, 'Tran Loc', '09-20-1999', 'f@gmail.com', 1, '09-20-2023', 1000, 4, 0),
	(7, 'Le Pham', '12-01-2000', 'g@gmail.com', 1, '09-20-2023', 1000, 5, 0)
GO

INSERT INTO SKILL (SkillName)
VALUES
	('Java'),
	('.NET'),
	('React'),
	('Angular'),
	('Database')
GO

INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate)
VALUES
	(1, 1, 1, '09-20-2023'),
	(1, 2, 2, '09-20-2023'),
	(2, 1, 1, '09-20-2023'),
	(3, 3, 1, '09-20-2023'),
	(2, 2, 1, '09-20-2023')
GO

/* Q4.2 */

CREATE VIEW EMPLOYEE_TRACKING
AS
SELECT E.EmpNo, E.EmpName, E.Level
FROM EMPLOYEE E
WHERE E.Level >= 3 AND E.Level <= 5
GO

SELECT * FROM EMPLOYEE_TRACKING